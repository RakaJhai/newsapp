### First App for React Native (NewsApp)

1. CLONE
    - SSH: git clone git@gitlab.com:RakaJhai/newsapp.git
    - HTTPS: git clone https://gitlab.com/RakaJhai/newsapp.git
2. npm install => to install all modules
    *** You can delete directory "/node_modules" before run npm install
3. npm start
4. npm react-native run-android or npm run android