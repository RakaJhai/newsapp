import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import NewsScreen from './src/screens/NewsScreen';

const App = () => (
  <View >
    <NewsScreen />
  </View>
);
export default App;